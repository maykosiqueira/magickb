const char *FW_VERSION="1.0.0.0";
const char *HW_ID="maykonKB";

String serialCmd = "";
bool flagControlRxSerial = false;

void setup() {
  // Configure serial port baudrate
  Serial.begin(9600);
  /* Limitando a 10 bytes para nossos comandos */
  serialCmd.reserve(10);
}

void loop() {
  /* Estou sempre verificando se acabou a recepcao da serial,
     esta variavel flagControlRxSerial se tornara true para isso.
  */
  if (flagControlRxSerial) {
    //Se o comando enviado f
    if ( serialCmd == "fwVer " ) {
      Serial.print(FW_VERSION);
    }
    else if ( serialCmd == "hwID ") {
      Serial.println(HW_ID);
    }
    else if ( serialCmd == "help " || serialCmd == "? ") {
      Serial.println("\tComandos : \n\thwID - Identificaçao atual de placa\n\tfwVer - Versao atual do firmware");
    }
    else {
      Serial.println("\t* Comando invalido *");
    }

    /* Resetando valores para nova recepcao */
    serialCmd = "";
    flagControlRxSerial = false;
  }
  
}


void serialEvent() {
  /* Loop verificando se algum byte esta disponivel na serial */
  while (Serial.available()) {

    /* O que chegar pela serial  feito um typecast de char para a variavel caractere */
    char caractere = (char)Serial.read();
    /* Nossa variavel caractere eh concatenada a nossa variavel serialCmd que eh uma String */
    serialCmd += caractere;

    /* Se chegar um espaço, nossa flag de controle (flagsControlRxSerial) passa para true
       e no loop principal ja podera ser utilizada
    */
    if(caractere == ' '){
      flagControlRxSerial = true;
    }
    
  }
  
}

