#ifndef DEVICENOTFOUND_H
#define DEVICENOTFOUND_H

#include <QWidget>
#include "serialport.h"


namespace Ui {
class deviceNotFound;
}

class deviceNotFound : public QWidget
{
    Q_OBJECT

public:
    explicit deviceNotFound(QWidget *parent = 0);
    ~deviceNotFound();

private slots:
    void on_cmdOK_clicked();

private:
    Ui::deviceNotFound *ui;
    serialPort *sp;
protected:
    void keyPressEvent(QKeyEvent *keyevent);

};

#endif // DEVICENOTFOUND_H
