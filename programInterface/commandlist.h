#ifndef COMMANDLIST_H
#define COMMANDLIST_H

#include <vector>
#include <QWidget>
#include <QPushButton>
#include <QMouseEvent>
#include <QSignalMapper>
#include <QMenu>

namespace Ui {
 class commandList;
}


class commandList : public QWidget
{
    Q_OBJECT
public:
    commandList(QWidget *parent);
    virtual ~commandList(){};
    void addCommand(int keycode, int keystatus);
    void changeCommand(int index, int keystatus);
    void deleteCommand(int index);
    void refreshWindow(bool del=false);
    int usedHeightPos();
    QList<int> *getKeyCode();
    QList<int> *getKeyStatus();
    QList<int> *getDelay();
    QList<QPushButton *> *getButtons();
    void configureButton(int index);
    void clearData();

public slots:
    void changeKeyCode(int key);
    void clicked(int key);
    void rightClick(int key);
private:
    void buttonRefreshStatus(QPushButton *button, int key);
    QList<int> keycode;
    QList<int> keystatus;
    QList<int> delay;
    QWidget *curForm;
    QList<QPushButton*> objs;
    QFont *font;
    const QString convertKey(int key);
    int buttonXPos;
    int buttonYPos;
    int buttonWidth;
    int buttonHeight;
    QSignalMapper *leftMapper;
    QSignalMapper *rightMapper;
    unsigned int i;
    unsigned int count;
    int y;
    int x;


};

#endif // COMMANDLIST_H
