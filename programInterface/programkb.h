#ifndef PROGRAMKB_H
#define PROGRAMKB_H

#include <QWidget>
#include <QPainter>
#include <QKeyEvent>
#include <QScrollArea>
#include <QFileDialog>
#include "commandlist.h"
#include "filemanager.h"
#include "databasemanager.h"

namespace Ui {
class programKB;
}

class programKB : public QWidget
{
    Q_OBJECT

public:
    explicit programKB(QWidget *parent = 0);

    ~programKB();


private slots:
    void on_verticalScrollBar_valueChanged(int value);

    void on_cmdExport_clicked();

    void on_cmdImport_clicked();

    void on_cmdClear_clicked();

    void on_cmdSave_clicked();

    void on_cmdImportTXT_clicked();

private:
    Ui::programKB *ui;
    commandList *commands;
    int lastScrollValue;
    void autoScroll();
    fileManager *manager;
    QFileDialog *dialog;
    databaseManager *db;
protected:
    void resizeEvent(QResizeEvent* event);
    void keyPressEvent(QKeyEvent *keyevent);
    void wheelEvent(QWheelEvent *event);
    void showEvent( QShowEvent* event );
};
#endif // PROGRAMKB_H
