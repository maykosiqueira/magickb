#ifndef GETKBKEY_H
#define GETKBKEY_H

#include <QWidget>

namespace Ui {
class getKbKey;
}

class getKbKey : public QWidget
{
    Q_OBJECT

public:
    bool getButton();
    explicit getKbKey(QWidget *parent = 0);
    ~getKbKey();

private:
    Ui::getKbKey *ui;
};

#endif // GETKBKEY_H
