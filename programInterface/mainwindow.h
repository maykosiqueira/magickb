#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "serialport.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_cmdOK_clicked();

    void on_cmdDeviceNotFound_clicked();

private:
    Ui::MainWindow *ui;
    serialPort *sp;
    QStringList ports;
protected:
    void keyPressEvent(QKeyEvent *keyevent);
};

#endif // MAINWINDOW_H
