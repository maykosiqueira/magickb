#ifndef SERIALPORT_H
#define SERIALPORT_H
#include <QStringList>

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>


class serialPort
{
public:
   serialPort();
   ~serialPort();
   QStringList loadDevices(QStringList *port);
   QString read();
   bool disconect();
   bool connect(QString Port, u_int32_t bd);
   bool checkDevice(QString port, int baudrate);
   bool checkDevice(QString port, int baudrate, QString *device);
   bool checkDevice(QString port, int baudrate, QStringList *device);
protected:
   QSerialPort *devSerial;
private:
   qint64 write(const char *cmd);
};

#endif // SERIALPORT_H
