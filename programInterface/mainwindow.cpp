#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "windowsfunctions.h"
#include <QMessageBox>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    sp = new serialPort();
    ui->setupUi(this);

    this->ui->cboDevices->addItems(sp->loadDevices(&ports));
    //this->ui->cboDevices->addItem("teste","Teste");
    //this->ui->cboDevices->addItem("Teste2");
}

MainWindow::~MainWindow()
{
    delete sp;
    delete ui;
}

void MainWindow::on_cmdOK_clicked()
{
    if(this->ui->cboDevices->currentIndex() == -1){
        QMessageBox::information(NULL,tr("Select Device"),tr("Please select a device to program."));
        return;
    }

    //Set Hardware ID
    hardwareID = this->ui->cboDevices->currentText();

    //Show de programation form of selected keyboard
    getKey->show();

    //Wait a button from keyboard
    if(getKey->getButton()){
        getKey->deleteLater();
        program->show();
    }else{
        getKey->close();
        this->close();
        QMessageBox::critical(NULL,tr("Get key"),tr("There was an error get the desired key, run the program again."));
    }

    //Close current form
    this->close();
}
void MainWindow::keyPressEvent(QKeyEvent *keyevent){
    // On press key return, automatic enter in program mode
    if(keyevent->key() == Qt::Key_Return){
        this->ui->cmdOK->click();
    }
}


void MainWindow::on_cmdDeviceNotFound_clicked()
{
    deviceNF->show();
    this->close();
}
