#include "filemanager.h"

#include <fstream>
#include <QTextStream>

fileManager::fileManager()
{

}


bool fileManager::write(QList<int> *code, QList<int> *status, const char *file){
    std::ofstream exportFile;
    //Open destiny file
    exportFile.open(file);
    //Check if file is open
    if(exportFile.is_open()){
        for(int i=0;i<code->size();i++)
        {
            exportFile << code->at(i);
            exportFile << ";";
            exportFile << status->at(i);
            exportFile << "\n";
        }
        exportFile.close();
        return true;
    }else{
        // If not open file, return false
        return false;
    }

}

bool fileManager::read(QWidget *form, commandList *command, const char *file){

    std::ifstream importFile;
    int length,n;
    const char *temp;
    int keycode=-1,keystatus=-1;
    bool statusChecked;
    QList<int> *code = command->getKeyCode();
    QList<int> *status = command->getKeyStatus();
    QList<QPushButton *> *button = command->getButtons();


    // Clear all current data
    command->clearData();

    importFile.open(file);
    if(importFile.is_open()){
        // Go to end of file
        importFile.seekg(0,std::ios::end);
        length = importFile.tellg();
        // Go to start of file
        importFile.seekg(0,std::ios::beg);

        char buffer[length];
        char bufKey[length-2];
        char bufStatus;

        while(!importFile.eof()){
            n=0;
            statusChecked = false;
            // Get a line
            importFile.getline(buffer,120);
            if(buffer[n] != '\0'){
                while(n != length){
                    // Get keycode and store
                    bufKey[n] = buffer[n];
                    if(buffer[n] == ';'){
                        //If detect delimiter, go to next line
                        bufKey[n] = '\0';
                        bufStatus = buffer[n+1];
                        // If detect delimiter triggers the status
                        statusChecked = true;
                        // Go to next line
                        break;
                    }
                    n++;
                }
                if(statusChecked){
                    //Convert char read from file to int
                    temp = bufKey;
                    keycode = atoi(temp);
                    temp = &bufStatus;
                    keystatus = atoi(temp);
                    // if keystatus was wrong, importation failed
                    if(keystatus < 0 || keystatus > 2){
                        importFile.close();
                        return false;
                    }
                    // Add imported data in list
                    *code << keycode;
                    *status << keystatus;
                    *button << new QPushButton(form);
                    //Configure button properties
                    command->configureButton(button->size()-1);
                }else{
                    importFile.close();
                    return false;
                }
            }

        }
        importFile.close();
        return true;
    }else{
        // If not open file, return false
        return false;
    }
}
bool fileManager::readTXT(QWidget *form, commandList *command, const char *file){

    int keycode=-1;
    // Get Structs
    QList<int> *code = command->getKeyCode();
    QList<int> *status = command->getKeyStatus();
    QList<int> *delay = command->getDelay();
    QList<QPushButton *> *button = command->getButtons();

    // Clear all current data
    command->clearData();
    //Open txt file
    QFile import(file);
    //Verify if txt is open
    if(import.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream in(&import);
        //Set unicode format
        in.setAutoDetectUnicode(true);
        QString line;
        //While not end of text
        while(!in.atEnd()){
            //Read line
            line = in.readLine();
            //Loop line at end
            for(int i = 0;i<line.size();i++){
                //Convert char read from file to int
                keycode = line.at(i).toUpper().unicode();
                //Debug
                //qDebug("Index: %i - Code:%i",i,keycode);
                //Store imported keys from txt to show
                *code << keycode;
                *status << 0;
                *delay << 250;
                *button << new QPushButton(form);
                //Configure button properties
                command->configureButton(button->size()-1);
            }
            if(!in.atEnd()){
                //Add carrier return if not final line
                *code << Qt::Key_Return;
                *status << 0;
                *delay << 250;
                *button << new QPushButton(form);
                //Configure button properties
                command->configureButton(button->size()-1);

            }
        }
        return true;
    }else{
        // If not open file, return false
        return false;
    }
}

