#ifndef WINDOWSFUNCTIONS_H
#define WINDOWSFUNCTIONS_H

#include <QWidget>
#include "programkb.h"
#include "getkbkey.h"
#include "devicenotfound.h"

void createWindows();
void disableResize(QWidget *form);

//It tells the system that the variable is global
extern programKB *program;
extern getKbKey *getKey;
extern deviceNotFound *deviceNF;
extern QString hardwareID;
extern QString hardwareButton;
extern QString portNumber;
extern int baudRate;

#endif // WINDOWSFUNCTIONS_H
