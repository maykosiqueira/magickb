#-------------------------------------------------
#
# Project created by QtCreator 2015-06-26T13:45:43
#
#-------------------------------------------------

QT       += core gui
QT       += sql
QT       += serialport
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MagicKb
TEMPLATE = app

CONFIG += console

SOURCES += main.cpp\
        mainwindow.cpp \
    programkb.cpp \
    windowsfunctions.cpp \
    commandlist.cpp \
    filemanager.cpp \
    databasemanager.cpp \
    getkbkey.cpp \
    serialport.cpp \
    devicenotfound.cpp

HEADERS  += mainwindow.h \
    programkb.h \
    windowsfunctions.h \
    commandlist.h \
    filemanager.h \
    databasemanager.h \
    getkbkey.h \
    serialport.h \
    devicenotfound.h

FORMS    += mainwindow.ui \
    programkb.ui \
    getkbkey.ui \
    devicenotfound.ui

TRANSLATIONS += language.ts
