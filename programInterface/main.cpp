#include "mainwindow.h"
#include "windowsfunctions.h"
#include "databasemanager.h"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // Define language translation
    QTranslator *translation = new QTranslator;

    QString file_translation;
    // Detect and load translation file according system config
    if(QLocale::system().language() == QLocale::Portuguese){
        file_translation = QApplication::applicationDirPath() + "/ptbr.qm";
    }

    // Check if translation are loaded correct
    if(!translation->load(file_translation)){
        delete translation;
    }else{
        qApp->installTranslator(translation);
    }

    //Create window
    MainWindow w;

    createWindows();
    w.setFixedSize(w.size() );
    w.show();

        return a.exec();
}
