#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QList>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QVariant>
#include "commandlist.h"

class databaseManager
{
public:
    databaseManager();
    ~databaseManager();
    bool updateKeyData(const char *hwID, const char *hwButton, QList<int> *keycode, QList<int> *keystatus, QList<int> *delay);
    bool deleteKeyData(const char *hwID,const char *hwButton);
    bool getKeyData(const char *hwID, const char *hwButton, commandList *command, QWidget *form);
private:
    bool open();
    void close();
    bool insertKeyData(const char *hwID, const char *hwButton, QList<int> *keycode, QList<int> *keystatus, QList<int> *delay);
    QString path;
    QSqlDatabase db;
    QSqlQuery *query;

};

#endif // DATABASEMANAGER_H
