#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QList>
#include <QString>
#include <QPushButton>
#include "commandlist.h"

class fileManager
{
public:
    fileManager();
    bool write(QList<int> *code, QList<int> *status, const char *file);
    bool read(QWidget *form, commandList *command,  const char * file);
    bool readTXT(QWidget *form, commandList *command,  const char * file);
};

#endif // FILEMANAGER_H
