#ifndef WINDOWFUNCTIONS
#define WINDOWFUNCTIONS

#include <QMainWindow>

void disableMaximize(QDialog *parent = 0);

#endif // WINDOWFUNCTIONS

