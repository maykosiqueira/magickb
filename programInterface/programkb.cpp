#include <QMessageBox>
#include "programkb.h"
#include "ui_programkb.h"
#include "windowsfunctions.h"

programKB::programKB(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::programKB)
{
    ui->setupUi(this);

    commands = new commandList(this->ui->list);
    // Set 0 value to start on top of window
    lastScrollValue = 0;
    // Set maximum scroll as 1000
    this->ui->verticalScrollBar->setMaximum(1000);
    // Start File manager
    manager = new fileManager();
    // Start and config file chooser dialog
    dialog = new QFileDialog(this);
    dialog->setNameFilter(tr("Keyboard (*.kb)"));
    dialog->setViewMode(QFileDialog::Detail);
    // Start database functions
    db = new databaseManager();
}
programKB::~programKB()
{
    delete ui;
}

void programKB::resizeEvent(QResizeEvent* event)
{
    // Return scrollbar status to 0
    this->ui->verticalScrollBar->setValue(0);

    // If detect window resize, resize the list and ajust label
    commands->refreshWindow();

    //Auto scroll down
    this->autoScroll();
}

void programKB::keyPressEvent(QKeyEvent *keyevent){
    // If a key pressed, add in list of commands
    commands->addCommand(keyevent->key(),0);
    commands->refreshWindow();
    //Auto scroll down
    this->autoScroll();
}

void programKB::on_verticalScrollBar_valueChanged(int value)
{
    //If scroll value reduce, scroll reduce in the widget with
    //objects, else increase
    this->ui->list->scroll(0,-10 * (value-lastScrollValue));
    //Get the current value to compare the scroll rolled up or down
    lastScrollValue = value;
}
void programKB::autoScroll(){
    if(this->ui->list->visibleRegion().boundingRect().height() != 0){
        int limiar, visibleArea;
        // Used area
        limiar = commands->usedHeightPos();

        visibleArea = this->ui->list->visibleRegion().boundingRect().height() + lastScrollValue * 10;
        while(limiar > visibleArea){
            //If the area used is larger than the visible, scroll
            lastScrollValue += 2;
            // Used area
            limiar = commands->usedHeightPos();
            //Show area
            visibleArea = this->ui->list->visibleRegion().boundingRect().height() + lastScrollValue * 10;
        }
        // Update scrollbar status
        this->ui->verticalScrollBar->setValue(lastScrollValue);
        // Update window scroll
        this->ui->list->scroll(0,lastScrollValue*-10);
    }

}
void programKB::wheelEvent(QWheelEvent *event)
{
    int value;
    //Calculate new scroll position
    value = this->ui->verticalScrollBar->value() - (event->delta()/120)*2;
    //Set new scroll position
    this->ui->verticalScrollBar->setValue(value);
}

void programKB::on_cmdExport_clicked()
{
    QString fileName;
    // Get file name and path to save
    fileName = dialog->getSaveFileName(this,tr("Save File"),"",tr("Keyboard (*.kb)"));
    fileName.replace(".kb","");
    // Verify if select a file
    if(!fileName.isEmpty()){
        fileName.append(".kb");
        //Save in file
        if(manager->write(commands->getKeyCode(),commands->getKeyStatus(),fileName.toStdString().c_str())){
            QMessageBox::information(NULL,tr("Export"),tr("Exported successfully"));
        }else{
            QMessageBox::critical(NULL,tr("Export"),tr("An error occurred when exporting"));
        }
    }
}

void programKB::on_cmdImport_clicked()
{
    QString fileName;
    // Get file name and path to save
    fileName = dialog->getOpenFileName(this,tr("Open File"),"",tr("Keyboard (*.kb)"));

    // Verify if select a file
    if(!fileName.isEmpty()){
        if(manager->read(this->ui->list,commands, fileName.toStdString().c_str())){
            // Return scrollbar status to 0
            this->ui->verticalScrollBar->setValue(0);
            // resize the list and ajust label
            commands->refreshWindow();
            //Auto scroll down
            this->autoScroll();

            QMessageBox::information(NULL,tr("Import"),tr("Imported successfully"));
        }else{
            QMessageBox::critical(NULL,tr("Import"),tr("There was an error importing the file \n Check the file format"));
        }
    }
}

void programKB::on_cmdClear_clicked()
{
    // Return scrollbar status to 0
    this->ui->verticalScrollBar->setValue(0);

    // Clear all data
    commands->clearData();

    // If detect window resize, resize the list and ajust label
    commands->refreshWindow();

    //Auto scroll down
    this->autoScroll();
}

void programKB::on_cmdSave_clicked()
{
    if(db->updateKeyData(hardwareID.toStdString().c_str(),hardwareButton.toStdString().c_str(),commands->getKeyCode(),commands->getKeyStatus(),commands->getDelay())){
        QMessageBox::information(NULL,tr("Save"),tr("The program was successfully saved"));
        this->close();
    }else{
        QMessageBox::critical(NULL,tr("Save"),tr("There was an error saving programming"));
    }
}
void programKB::showEvent( QShowEvent* event ){
    db->getKeyData(hardwareID.toStdString().c_str(),hardwareButton.toStdString().c_str(),commands,this->ui->list);
    commands->refreshWindow();
    autoScroll();
}

void programKB::on_cmdImportTXT_clicked()
{
    QString fileName;
    // Get file name and path to save
    fileName = dialog->getOpenFileName(this,tr("Open File"),"",tr("Text File (*.txt)"));

    // Verify if select a file
    if(!fileName.isEmpty()){
        if(manager->readTXT(this->ui->list,commands, fileName.toStdString().c_str())){
            // Return scrollbar status to 0
            this->ui->verticalScrollBar->setValue(0);
            // resize the list and ajust label
            commands->refreshWindow();
            //Auto scroll down
            this->autoScroll();

            QMessageBox::information(NULL,tr("Import"),tr("Imported successfully"));
        }else{
            QMessageBox::critical(NULL,tr("Import"),tr("There was an error importing the file \n Check the file format"));
        }
    }
}
