#include "databasemanager.h"

databaseManager::databaseManager()
{
    // Set database file name
    path = "store.db";
    //Set data base format
    db = QSqlDatabase::addDatabase("QSQLITE");
    // Set path of database file
    db.setDatabaseName(path);

    if(this->open()){
        //Create table if not exist
        query->prepare("CREATE TABLE IF NOT EXISTS \"keys\"(\
                     'hwID' TEXT NOT NULL,\
                     'hwButton' TEXT NOT NULL,\
                     'keycode' INTEGER NOT NULL,\
                     'keystatus' INTEGER NOT NULL,\
                     'delay' INTEGER NOT NULL)");
        query->exec();
        this->close();
    }
}
databaseManager::~databaseManager(){
    delete query;
}

bool databaseManager::open(){
    if(db.open( )){ // If sucess on open database return true
        //db.setPassword("**M4gicK3y))");
        // Create new instance query
        query = new QSqlQuery(db);
        return true;
    }else{ // else return false
        return false;
    }
}

void databaseManager::close(){
    // Destroy query
    delete query;
    //Compact database
    db.exec("VACUUM");
    //Close db
    db.close();
}
bool databaseManager::updateKeyData(const char *hwID,const char *hwButton,QList<int> *keycode,QList<int> *keystatus, QList<int> *delay){
    // To update a button program, clear previous programation
    if(!this->deleteKeyData(hwID,hwButton)){
        return false;
    }
    // Insert new programation
    if(!this->insertKeyData(hwID,hwButton,keycode,keystatus, delay)){
        return false;
    }
    return true;
}

bool databaseManager::deleteKeyData(const char *hwID,const char *hwButton){
        //Open db connection
        if(this->open()){
            // Query to delete all data to reprogram key
            query->prepare("delete from `keys` where hwID=:id and hwButton=:btn;");
            query->bindValue(":id",hwID);
            query->bindValue(":btn",hwButton);
            // Execute query
            query->exec();
            //Close db connection
            this->close();
            return true;
        }else{
            return false;
        }
}

bool databaseManager::getKeyData(const char *hwID,const char *hwButton, commandList *command, QWidget *form){
    //Data of program
    QList<int> *code = command->getKeyCode();
    QList<int> *status = command->getKeyStatus();
    QList<int> *delay = command->getDelay();

    QList<QPushButton *> *button = command->getButtons();

    if(this->open()){
        query->prepare("select * from `keys` where hwID=:id and hwButton=:btn;");
        query->bindValue(":id", hwID);
        query->bindValue(":btn",hwButton);
        query->exec();
        while(query->next()){
            // Add imported data in list
            *code << query->value(2).toInt();
            *status << query->value(3).toInt();
            *delay << query->value(4).toInt();
            *button << new QPushButton(form);
            //Configure button properties
            command->configureButton(button->size()-1);
        }
        return true;
    }else{
        return false;
    }
}

bool databaseManager::insertKeyData(const char *hwID,const char *hwButton,QList<int> *keycode,QList<int> *keystatus, QList<int> *delay){
    int count=0;
    QVariantList code,status, ID,Button, delays;
    //Open db connection
    if(this->open()){
        db.transaction();
        // Loop all keycode and add in database
        // Prepare query to insert data
        query->prepare("INSERT INTO `keys`(`hwID`,`hwButton`,`keycode`,`keystatus`, `delay`) VALUES (?,?,?,?,?);");
        foreach(int value, *keycode){
            // Set Hardware ID and Button to program
            ID << hwID;
            Button << hwButton;
            // Add code and status to list
            code << value;
            status << keystatus->at(count);
            delays << delay->at(count);
            // Count to next status
            count ++;
        }
        // Add lists to sql statement
        query->addBindValue(ID);
        query->addBindValue(Button);
        query->addBindValue(code);
        query->addBindValue(status);
        query->addBindValue(delays);
        // Execute sql
        query->execBatch();
        db.commit();
        //Close db connection
        this->close();
        return true;

    }else{
        return false;
    }

}
