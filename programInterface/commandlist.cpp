#include "commandlist.h"
#include <string>
#include <stdio.h>

commandList::commandList(QWidget *parent) : QWidget(parent)
{
    curForm = parent;
    // Map of function of buttons when left mouse button click
    leftMapper = new QSignalMapper(this);
    QObject::connect(leftMapper, SIGNAL(mapped(int)),this,SLOT(clicked(int)));

    // Map of function of buttons when right mouse button click
    rightMapper = new QSignalMapper(this);
    QObject::connect(rightMapper, SIGNAL(mapped(int)),this,SLOT(rightClick(int)));

    //Create a new Font
     font = new QFont("times",12);
     font->setBold(true);

}
void commandList::addCommand(int keycode, int keystatus){
    //Add new key to vector and your status
    this->keycode << keycode;
    this->keystatus << keystatus;
    this->delay << 250;
    this->objs << new QPushButton(curForm);

    //Configure button setting
    configureButton(objs.size()-1);
}

void commandList::changeCommand(int index, int keystatus){
    //Change status of selected key
    this->keystatus[index] = keystatus;
}

void commandList::deleteCommand(int index){
    // Remove from QList the key selected
    keycode.removeAt(index);
    keystatus.removeAt(index);
    delay.removeAt(index);

    //Remove button from QList to not show the user
    delete objs.takeAt(index);
    refreshWindow(true);
}

void commandList::refreshWindow(bool del){
    i = 0;
    count = 0;

    //Calculate 1% of screen height to start place the caracters
    y = curForm->height()/100;
    //Calculate 1% of screen width to start place the caracters
    x = curForm->width()/100;

    buttonXPos = x;
    buttonYPos = y;

    foreach(QPushButton *button, objs){
        // Set button size
        buttonWidth = button->sizeHint().width() + 10;
        buttonHeight = button->sizeHint().height();

        if(buttonXPos + buttonWidth > curForm->width())
        {
            //echo in new line the caracters
            y ++;
            //Go to begin of new line
            buttonYPos = buttonYPos +buttonHeight;
            buttonXPos = x;
        }

        //Set new width and height of label
        button->setGeometry(QRect(buttonXPos,buttonYPos,buttonWidth,buttonHeight));

        // if receive a delete signal, update all callback
        if(del){
            leftMapper->removeMappings(leftMapper);
            rightMapper->removeMappings(rightMapper);

            // Update Callback left click index
            leftMapper->setMapping(button, count);
            // Updater Callback right click index
            rightMapper->setMapping(button, count);
        }



        //Go to next letter
        buttonXPos += buttonWidth;
        i++;
        count ++;
    }
}
void commandList::changeKeyCode(int key){
    // Change status code
    if(keystatus[key] == 0){
        keystatus[key] = 1;
    }else if(keystatus[key] == 1){
       keystatus[key] = 2;
    }else if(keystatus[key] == 2){
        keystatus[key] = 0;
    }
    // Refresh button color, according key status
    buttonRefreshStatus(objs[key], key);
}
void commandList::buttonRefreshStatus(QPushButton *button, int key){

    if(keystatus[key] == 0){
        //If key press set the background as green and letter as white
        button->setStyleSheet("QPushButton {\
                              color: white;\
                              background-color: qlineargradient(spread:pad, x1:0, y1:1, x2:0, y2:0, stop:0 rgba(218,165,32), \
                              stop:1 rgba(184,134,11));\
                              border-style:outset;\
                              border-radius: 7px;\
                              border-color: transparent;border-width: 1px;}");
    }else if(keystatus[key] == 1){
        //If key hold set the background as red and letter as white
    button->setStyleSheet("QPushButton {\
                                color: white;\
                                background-color: qlineargradient(spread:pad, x1:0, y1:1, x2:0, y2:0, stop:0 rgba(255,165,0), \
                                stop:1 rgba(139,0,0));\
                                border-style:outset;\
                                border-radius: 7px;\
                                border-color: transparent;border-width: 1px;}");
    }else if(keystatus[key] == 2){
        //If key release set the background as blue and letter as white
    button->setStyleSheet("QPushButton {\
                                color: white;\
                                background-color: qlineargradient(spread:pad, x1:0, y1:1, x2:0, y2:0, stop:0 rgba(64,224,208), \
                                stop:1 rgba(0,0,139));\
                                border-style:outset;\
                                border-radius: 7px;\
                                border-color: transparent;border-width: 1px;}");
    }
}

void commandList::clicked(int key){
    //On click in the button, change status and button color
    changeKeyCode(key);
}
const QString commandList::convertKey(int key){

    char letter[2];
    const char *caracter;

    // Keymap to show to the user
    if(key== Qt::Key_Ccedilla){
        return tr("Ç");
    }else if(key == Qt::Key_Control){
        return tr("[CTRL]");
    }else if(key == Qt::Key_Shift){
        return tr("[SHIFT]");
    }else if(key == Qt::Key_Space){
       return tr("[SPACE]");
    }else if(key == Qt::Key_Alt){
        return tr("[ALT]");
    }else if(key == Qt::Key_Tab){
        return tr("[TAB]");
    }else if(key == Qt::Key_Backspace){
        return tr("[BACKSPACE]");
    }else if(key == Qt::Key_Return){
        return tr("[RETURN]");
    }else if(key == Qt::Key_F1){
        return tr("[F1]");
    }else if(key == Qt::Key_F2){
        return tr("[F2]");
    }else if(key == Qt::Key_F3){
        return tr("[F3]");
    }else if(key == Qt::Key_F4){
        return tr("[F4]");
    }else if(key == Qt::Key_F5){
        return tr("[F5]");
    }else if(key == Qt::Key_F6){
        return tr("[F6]");
    }else if(key == Qt::Key_F7){
        return tr("[F7]");
    }else if(key == Qt::Key_F8){
        return tr("[F8]");
    }else if(key == Qt::Key_F9){
        return tr("[F9]");
    }else if(key == Qt::Key_F10){
        return tr("[F10]");
    }else if(key == Qt::Key_F11){
        return tr("[F11]");
    }else if(key == Qt::Key_F12){
        return tr("[F12]");
    }else if(key == Qt::Key_AltGr){
        return tr("[ALTGR]");
    }else if(key == Qt::Key_Left){
        return tr("[LEFT]");
    }else if(key == Qt::Key_Right){
        return tr("[RIGHT]");
    }else if(key == Qt::Key_Up){
        return tr("[UP]");
    }else if(key == Qt::Key_Down){
        return tr("[DOWN]");
    }else if(key == Qt::Key_Delete){
        return tr("[DELETE]");
    }else if(key == Qt::Key_CapsLock){
        return tr("[CAPSLOCK]");
    }else if(key == Qt::Key_Escape){
        return tr("[ESCAPE]");
    }else if(key == Qt::Key_Insert){
        return tr("[INSERT]");
    }else if(key == Qt::Key_Pause){
        return tr("[PAUSE]");
    }else if(key == Qt::Key_Aacute){
        return tr("Á");
    }else if(key == Qt::Key_Eacute){
        return tr("É");
    }else if(key == Qt::Key_Iacute){
        return tr("Í");
    }else if(key == Qt::Key_Oacute){
        return tr("Ó");
    }else if(key == Qt::Key_Uacute){
        return tr("Ú");
    }else if(key == Qt::Key_Atilde){
        return tr("Ã");
    }else if(key == Qt::Key_Agrave){
        return tr("À");
    }else if(key == Qt::Key_Otilde){
        return tr("Õ");
    }else if(key == Qt::Key_Egrave){
        return tr("É");
    }else if(key == Qt::Key_Igrave){
        return tr("Ì");
    }else if(key == Qt::Key_Ograve){
        return tr("Ò");
    }else if(key == Qt::Key_Ugrave){
        return tr("Ù");
    }else if(key == Qt::Key_Udiaeresis){
        return tr("Ü");
    }else if(key == Qt::Key_degree){
        return tr("°");
    }else if(key == Qt::Key_section){
        return tr("§");
    }else if(key == Qt::Key_cent){
        return tr("¢");
    }else if(key == Qt::Key_sterling){
        return tr("£");
    }else if(key == Qt::Key_threesuperior){
        return tr("³");
    }else if(key == Qt::Key_twosuperior){
        return tr("²");
    }else if(key == Qt::Key_onesuperior){
        return tr("¹");
    }else if(key == Qt::Key_notsign){
        return tr("¬");
    }else if(key == Qt::Key_Ecircumflex){
        return tr("Ê");
    }else if(key == Qt::Key_notsign){
        return tr("¬");
    }else if(key == Qt::Key_Acircumflex){
        return tr("Â");
    }else if(key == Qt::Key_Ecircumflex){
        return tr("Ê");
    }else if(key == Qt::Key_Icircumflex){
        return tr("Î");
    }else if(key == Qt::Key_Ocircumflex){
        return tr("Ô");
    }else if(key == Qt::Key_Ucircumflex){
        return tr("Û");
    }else{
        letter[0] = key;
        letter[1] = '\0';
        caracter = letter;
        return caracter;
    }
}
void commandList::rightClick(int key)
{
   deleteCommand(key);
}
int commandList::usedHeightPos(){
    return buttonYPos + buttonHeight;
}

QList<int> *commandList::getKeyCode(){
    return &keycode;
}

QList<int> *commandList::getKeyStatus(){
    return &keystatus;
}

QList<int> *commandList::getDelay(){
    return &delay;
}

QList<QPushButton*> *commandList::getButtons(){
    return &objs;;
}
void commandList::configureButton(int index){
    // Set button feature
    objs.at(index)->setAutoFillBackground(true);
    // Set key text
    objs.at(index)->setText(convertKey(this->keycode[index]));
    // Disable focus after click in button
    objs.at(index)->setFocusPolicy(Qt::NoFocus);
    // Set button color
    buttonRefreshStatus(objs.at(index),index);
    // Add right click
    objs.at(index)->setContextMenuPolicy(Qt::CustomContextMenu);
    // Set font of button
    objs.at(index)->setFont(*font);
    //Show button with the new features
    objs.at(index)->show();
    // Associate function to the button
    QObject::connect(objs.at(index), SIGNAL(clicked()), leftMapper, SLOT(map()));
    // Associate function to the button
    QObject::connect(objs.at(index), SIGNAL(customContextMenuRequested(const QPoint)), rightMapper, SLOT(map()));

    // Update Callback left click index
    leftMapper->setMapping(objs.at(index), index);
    // Updater Callback right click index
    rightMapper->setMapping(objs.at(index), index);
}

void commandList::clearData(){
    // Remove all keycode and status
    this->keycode.clear();
    this->keystatus.clear();
    this->delay.clear();
    //Destroy all buttons and clear list
    qDeleteAll(objs.begin(), objs.end());
    objs.clear();
}
