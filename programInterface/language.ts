<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Selecting devices</source>
        <translation>Selecione seu dispositivo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="41"/>
        <source>If you can not see your device click here</source>
        <translation>Falta dispositivo? clique aqui</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="51"/>
        <source>Select your device</source>
        <translation>Dispositivo:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="93"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="29"/>
        <source>Select Device</source>
        <oldsource>Selected Device</oldsource>
        <translation>Selecione o dispositivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="29"/>
        <source>Please select a device to program.</source>
        <translation>Por favor, selecione um dispositivo a ser programado.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="48"/>
        <source>Get key</source>
        <translation>Captura de tecla</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="48"/>
        <source>There was an error get the desired key, run the program again.</source>
        <translation>Ocorreu um erro ao capturar a tecla desejada, rode o programa novamente.</translation>
    </message>
</context>
<context>
    <name>commandList</name>
    <message>
        <location filename="commandlist.cpp" line="151"/>
        <source>Ç</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="153"/>
        <source>[CTRL]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="155"/>
        <source>[SHIFT]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="157"/>
        <source>[SPACE]</source>
        <translation>[ESPACO]</translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="159"/>
        <source>[ALT]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="161"/>
        <source>[TAB]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="163"/>
        <source>[BACKSPACE]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="165"/>
        <source>[RETURN]</source>
        <translation>[ENTER]</translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="167"/>
        <source>[F1]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="169"/>
        <source>[F2]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="171"/>
        <source>[F3]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="173"/>
        <source>[F4]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="175"/>
        <source>[F5]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="177"/>
        <source>[F6]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="179"/>
        <source>[F7]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="181"/>
        <source>[F8]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="183"/>
        <source>[F9]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="185"/>
        <source>[F10]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="187"/>
        <source>[F11]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="189"/>
        <source>[F12]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="191"/>
        <source>[ALTGR]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="193"/>
        <source>[LEFT]</source>
        <translation>[ESQUERDA]</translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="195"/>
        <source>[RIGHT]</source>
        <translation>[DIREITA]</translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="197"/>
        <source>[UP]</source>
        <translation>[CIMA]</translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="199"/>
        <source>[DOWN]</source>
        <translation>[BAIXO]</translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="201"/>
        <source>[DELETE]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="203"/>
        <source>[CAPSLOCK]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="205"/>
        <source>[ESCAPE]</source>
        <translation>[ESC]</translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="207"/>
        <source>[INSERT]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="209"/>
        <source>[PAUSE]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="211"/>
        <source>Á</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="213"/>
        <source>É</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="215"/>
        <source>Í</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="217"/>
        <source>Ó</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="219"/>
        <source>Ú</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="221"/>
        <source>Ã</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="223"/>
        <source>Õ</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="225"/>
        <source>°</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="227"/>
        <source>§</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="229"/>
        <source>¢</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="231"/>
        <source>£</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="233"/>
        <source>³</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="235"/>
        <source>²</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="237"/>
        <source>¹</source>
        <translation></translation>
    </message>
    <message>
        <location filename="commandlist.cpp" line="239"/>
        <source>¬</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>deviceNotFound</name>
    <message>
        <location filename="devicenotfound.ui" line="14"/>
        <source>Manual port</source>
        <translation>Porta Manual</translation>
    </message>
    <message>
        <location filename="devicenotfound.ui" line="26"/>
        <source>Port</source>
        <translation>Porta</translation>
    </message>
    <message>
        <location filename="devicenotfound.ui" line="47"/>
        <source>BaudRate</source>
        <translation>Baud Rate</translation>
    </message>
    <message>
        <location filename="devicenotfound.ui" line="73"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="devicenotfound.cpp" line="27"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="devicenotfound.cpp" line="27"/>
        <source>The informed settings are invalid, please check the information and try again.</source>
        <translation>As configurações informadas são inválidas, verifique as informações e tente novamente.</translation>
    </message>
</context>
<context>
    <name>getKbKey</name>
    <message>
        <location filename="getkbkey.ui" line="14"/>
        <source>Keyboard</source>
        <translation>Teclado</translation>
    </message>
    <message>
        <location filename="getkbkey.ui" line="26"/>
        <source>Please press the button on your programmable keyboard you wish to program...</source>
        <translation>Por favor, pressione o botão em seu teclado que deseja programar...</translation>
    </message>
</context>
<context>
    <name>programKB</name>
    <message>
        <location filename="programkb.ui" line="14"/>
        <source>Program your keyboard</source>
        <translation>Programe seu teclado</translation>
    </message>
    <message>
        <location filename="programkb.ui" line="98"/>
        <source>Import Program</source>
        <translation>Importar programação</translation>
    </message>
    <message>
        <location filename="programkb.ui" line="169"/>
        <source>Export program</source>
        <translation>Exportar programação</translation>
    </message>
    <message>
        <location filename="programkb.ui" line="189"/>
        <source>Import TXT</source>
        <translation>Importar TXT</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="124"/>
        <location filename="programkb.cpp" line="126"/>
        <location filename="programkb.cpp" line="177"/>
        <location filename="programkb.cpp" line="179"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="programkb.ui" line="138"/>
        <source>Clear All</source>
        <translation>Limpar tudo</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="101"/>
        <location filename="programkb.cpp" line="103"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="programkb.ui" line="118"/>
        <source>Save and Close</source>
        <translation>Salvar e Sair</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="21"/>
        <location filename="programkb.cpp" line="94"/>
        <location filename="programkb.cpp" line="112"/>
        <source>Keyboard (*.kb)</source>
        <translation>Teclado (*.kb)</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="94"/>
        <source>Save File</source>
        <translation>Salvar arquivo</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="101"/>
        <source>Exported successfully</source>
        <translation>Exportado com sucesso</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="103"/>
        <source>An error occurred when exporting</source>
        <translation>Ocorreu um erro ao exportar</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="112"/>
        <location filename="programkb.cpp" line="165"/>
        <source>Open File</source>
        <translation>Abrir arquivo</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="124"/>
        <location filename="programkb.cpp" line="177"/>
        <source>Imported successfully</source>
        <translation>Importado com sucesso</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="126"/>
        <location filename="programkb.cpp" line="179"/>
        <source>There was an error importing the file 
 Check the file format</source>
        <translation>Ocorreu um erro ao importar o arquivo
Verifique o formato</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="149"/>
        <location filename="programkb.cpp" line="152"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="149"/>
        <source>The program was successfully saved</source>
        <translation>A programação foi salva com sucesso</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="152"/>
        <source>There was an error saving programming</source>
        <translation>Ocorreu um erro durante o salvamento da programação</translation>
    </message>
    <message>
        <location filename="programkb.cpp" line="165"/>
        <source>Text File (*.txt)</source>
        <translation>Arquivo de texto (*.txt)</translation>
    </message>
    <message>
        <source>There was an error importing the file  nCheck the file format</source>
        <translation type="vanished">Ocorreu um erro ao importar o arquivo\nVerifique o formato</translation>
    </message>
</context>
</TS>
