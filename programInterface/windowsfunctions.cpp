#include "windowsfunctions.h"

/*
 * This method is to show a unique time a form, when the form are closed
 * you state is saved.
*/


// Store hardware and button id
QString hardwareID;
QString hardwareButton = "teste";
QString portNumber;
int baudRate = 9600;

//Declarate the forms
programKB *program;
getKbKey *getKey;
deviceNotFound *deviceNF;


void createWindows(){
    //intance the forms for the future use
    getKey = new getKbKey;
    program = new programKB;
    deviceNF = new deviceNotFound;

    // Disable resize window
    disableResize(deviceNF);
    disableResize(getKey);

}

void disableResize(QWidget *form){
    //Disable QWidget form resize set the max and min was the current value
    form->setMaximumSize(form->size());
}
