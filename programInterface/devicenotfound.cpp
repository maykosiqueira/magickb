#include "devicenotfound.h"
#include "ui_devicenotfound.h"
#include <QMessageBox>
#include "windowsfunctions.h"

deviceNotFound::deviceNotFound(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::deviceNotFound)
{
    ui->setupUi(this);
    sp = new serialPort();
}

deviceNotFound::~deviceNotFound()
{
    delete ui;
}

void deviceNotFound::on_cmdOK_clicked()
{
    if(sp->checkDevice(ui->txtPort->text(),ui->txtBR->text().toInt(), &hardwareID)){
        portNumber = ui->txtPort->text();
        baudRate = ui->txtBR->text().toInt();
        getKey->show();
        //Wait a button from keyboard
        if(getKey->getButton()){
            getKey->deleteLater();
            program->show();
        }else{
            getKey->close();
            this->close();
            QMessageBox::critical(NULL,tr("Get key"),tr("There was an error get the desired key, run the program again."));
        }
        this->close();
    }else{
        QMessageBox::critical(NULL,tr("Device"),tr("The informed settings are invalid, please check the information and try again."));
    }
}
void deviceNotFound::keyPressEvent(QKeyEvent *keyevent){
    // On press key return, automatic enter in program mode
    if(keyevent->key() == Qt::Key_Return){
        this->ui->cmdOK->click();
    }
}
