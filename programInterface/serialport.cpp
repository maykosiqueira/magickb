#include "serialport.h"
#include "windowsfunctions.h"
#include <QThread>


serialPort::serialPort()
{
 devSerial = new QSerialPort();
}
serialPort::~serialPort(){
    delete devSerial;
}

// Generate 2 QStringList with port and devices name
QStringList serialPort::loadDevices(QStringList *port)
{
    QStringList device;

    foreach (const QSerialPortInfo &devinfo, QSerialPortInfo::availablePorts()) {
        QSerialPort devserial;
        devserial.setPort(devinfo);
        // Connect the serial port to check if it is a keyboard
        if (devserial.open(QIODevice::ReadWrite)) {
            //Get port of device
            *port << devinfo.portName();
            devserial.close();
            //Check if a device is valid, if not, remove stored port
            // if true, get hardware id
            if(!checkDevice(port->takeAt(port->size()-1),baudRate,&device)){
                //Remove add port on error to connect
               port->removeAt(port->size()-1);
            }
        }
    }
    return device;

}
//Open a connection with keyboard
bool serialPort::connect(QString Port, u_int32_t bd)
{
    /* Device Serial Port */
    devSerial->setPortName(Port);

     /* Connect SerialPort */
        /* BaudRate */
        switch (bd) {
        case 2400:
            devSerial->setBaudRate(QSerialPort::Baud2400);
            break;
        case 4800:
            devSerial->setBaudRate(QSerialPort::Baud4800);
            break;
        case 9600:
            devSerial->setBaudRate(QSerialPort::Baud9600);
            break;
        case 19200:
            devSerial->setBaudRate(QSerialPort::Baud19200);
            break;
        case 115200:
            devSerial->setBaudRate(QSerialPort::Baud115200);
            break;
        default:
            qDebug("Invalid BaudRate\nUse: 2400 - 4800 - 9600 -19200 - 115200");
            return false;
        }

        /* FlowControl */
        devSerial->setFlowControl(QSerialPort::NoFlowControl);

        /* Configurações adicionais */
        devSerial->setDataBits(QSerialPort::Data8);
        devSerial->setParity(QSerialPort::NoParity);
        devSerial->setStopBits(QSerialPort::OneStop);

        if(devSerial->open(QIODevice::ReadWrite)) {
            // Serial port is open sucess
            return true;
        }
        else {
            // Error on open serial port
            qDebug("Error on open serial port: %s",devSerial->errorString().toStdString().c_str()) ;
            return false;
        }
}

//Close connection
bool serialPort::disconect()
{
    devSerial->clear();
    devSerial->close();
    if(devSerial->error() == 0 || !devSerial->isOpen()) {
        return true;
    }
    else {
        // Erro on close serial port
        qDebug("Error on close serial port: %s",devSerial->errorString().toStdString().c_str()) ;
        return false;
    }
}

// Read data from keyboard
QString serialPort::read()
{
     QString bufRxSerial = "";

     /* Awaits read all the data before continuing */
     while (devSerial->waitForReadyRead(20)) {
         bufRxSerial += devSerial->readAll();
     }
     return bufRxSerial;
}

//Send a command to keyboard
qint64 serialPort::write(const char *cmd)
{
    qint64 lenght=-1;
    lenght = devSerial->write(cmd,qstrlen(cmd));
    return lenght;
}


// This function checks if able to connect to the device
// on that port and the baud rate and check the firmware version is correct
bool serialPort::checkDevice(QString port, int baudrate){
    // test connection with port and baudrate set
    if(connect(port,baudrate)){
        //Delay 2 seconds to serial port work propertly
        QThread::sleep(2);
        write("fwVer ");
        // If firmware version was wrong, system dont start
        if(read() != "1.0.0.0"){
            disconect();
            qDebug("Wrong firmware version.");
            return false;
        }
       if(!disconect()){
            // If not disconect, return false
            return false;
        }
        return true;
    }else{
        return false;
    }
}

// This function checks if able to connect to the device
// on that port and the baud rate and check the firmware version is correct
// and get device id
bool serialPort::checkDevice(QString port, int baudrate, QString *device){
    // test connection with port and baudrate set
    if(connect(port,baudrate)){
        //Delay 2 seconds to serial port work propertly
        QThread::sleep(2);
        write("fwVer ");
        // If firmware version was wrong, system dont start
        if(read() != "1.0.0.0"){
            disconect();
            qDebug("Wrong firmware version.");
            return false;
        }
        //Send a command to the hardware return your HardwareID
        write("hwID ");
        // Store hardware id returned
        *device = read();
        if(!disconect()){
            // If not disconect, return false
            return false;
        }
        return true;
    }else{
        return false;
    }
}

// This function checks if able to connect to the device
// on that port and the baud rate and check the firmware version is correct
// and get device id
bool serialPort::checkDevice(QString port, int baudrate, QStringList *device){
    // test connection with port and baudrate set
    if(connect(port,baudrate)){
        //Delay 2 seconds to serial port work propertly
        QThread::sleep(2);
        write("fwVer ");
        // If firmware version was wrong, system dont start
        if(read() != "1.0.0.0"){
            disconect();
            qDebug("Wrong firmware version.");
            return false;
        }
        //Send a command to the hardware return your HardwareID
        write("hwID ");
        // Store hardware id returned
        *device << read();
        if(!disconect()){
            // If not disconect, return false
            return false;
        }
        return true;
    }else{
        return false;
    }
}
